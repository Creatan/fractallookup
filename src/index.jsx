import React from 'react';
import ReactDOM from 'react-dom';
import FractalLookup from './components/FractalLookup';

require('./css/style.css');

let FRACTALS = {
    'Aquatic Ruins':[3, 18, 26, 42, 61, 75],
    'Swampland':[2, 21, 32, 41, 56, 67, 77, 83],
    'Uncategorized':[4, 12, 36, 44, 62, 79, 91],
    'Urban Battleground':[1, 11, 31, 57, 66, 78, 85],
    'Molten Furnace':[9, 23, 39, 58, 73, 89, 93],
    'Snowblind':[5, 16, 27, 37, 51, 74, 86],
    'Cliffside':[7, 13, 22, 33, 47, 59, 69, 82, 94],
    'Undeground Facility':[8, 17, 29, 38, 43, 53, 68, 76, 81, 95],
    'Aetherblade':[14, 24, 49, 54, 65, 71, 88, 96],
    'Thaumanova Reactor':[15, 34, 48, 55, 64, 84, 97],
    'Volcanic':[6, 19, 28, 46, 52, 63, 72, 87, 92],
    'Captain Mai Trin':[25, 50, 100],
    'Molten Boss':[10, 40, 80, 90, 99],
    'Solid Ocean':[20, 30, 35, 45, 60, 70, 98]
};


ReactDOM.render(
    <FractalLookup fractals={FRACTALS}/>,
    document.getElementById('app')
);