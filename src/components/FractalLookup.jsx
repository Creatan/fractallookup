import React from 'react';
import FractalList from './FractalList';
import Search from './Search';



class FractalLookup extends React.Component {
    constructor(props) {
        super(props);
        this.displayName = 'FractalLookup';
        this.handleChange = this.handleChange.bind(this);       
        this.state = {
            levelSearch: []
        }
    }
    handleChange(levelString){
        let levels = levelString.split(',');
        let levelsLookup = [];
        levels.forEach(function(level){
            if(level !==''){
                levelsLookup.push(level);
            }
        });
        
        this.setState({
            levelSearch: levelsLookup
        });
    }
    render() {
        return (
            <div>
                <Search 
                    onUserInput={this.handleChange.bind(this)}
                />
                <FractalList 
                    fractals={this.props.fractals} 
                    displayLevels={this.state.levelSearch} 
                />
            </div>
        );
    }
}

export default FractalLookup;
