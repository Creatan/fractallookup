import React from 'react';

class Fractal extends React.Component {
    constructor(props) {
        super(props);
        this.displayName = 'Fractal';
    }
    render() {
        return (
            <li>
                <div className="level">
                    {this.props.fractal.level}
                </div>
                <div className="name">
                    {this.props.fractal.name}
                </div>
            </li>
        );
    }
}

export default Fractal;
