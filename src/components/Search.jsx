import React from 'react';
var ENTER_KEY = 13;

class Search extends React.Component {
    constructor(props) {
        super(props);
        this.displayName = 'Search';
    }
    handleChange(event){
        if (event.keyCode !== ENTER_KEY){
            return;
        }
        event.preventDefault();
        //allow only numbers and commas
        let val = this.refs.levelFilterInput.value.replace(/[^,0-9]/g,'');
        this.refs.levelFilterInput.value = val;

        this.props.onUserInput(val);
    }
    render() {
        return (
            <div>
                <input 
                    ref="levelFilterInput" 
                    type="text" 
                    name="level" 
                    placeholder="Give a fractal level" 
                    onKeyDown={this.handleChange.bind(this)}
                    autoFocus={true}
                />
            </div>
        );
    }
}

export default Search;
