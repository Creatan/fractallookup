import React from 'react';
import Fractal from './Fractal'

class FractalList extends React.Component {
    constructor(props) {
        super(props);
        this.displayName = 'FractalList';
    }
    render() {
        let rows = [];
        
        this.props.displayLevels.forEach(function(level){
            
            
            
            for(let fractal in this.props.fractals ){
                if(this.props.fractals[fractal].indexOf(parseInt(level)) > -1){
                    let selectedFractal = {
                        level: level,
                        name: fractal
                    };
                    rows.push(<Fractal fractal={selectedFractal} key={selectedFractal.level} />);
                    break;
                }
            }

            
            
        }.bind(this));

        return (
            <ul>
                {rows}
            </ul>
        );
    }
}

export default FractalList;
